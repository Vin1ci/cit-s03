package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;

	private String contentType;
	
	public void init() throws ServletException{
		contentType ="index/html";
		System.out.println("***************************");
		System.out.println(" Initialized connection to DB ");
		System.out.println("***************************");
	}
	
	public void destroy() {
		System.out.println("***************************");
		System.out.println(" Disconnected from DB ");
		System.out.println("***************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		int total = 0;
		
		String op = req.getParameter("operation");
		if(op.equals("add")) total = num1 + num2;
		if(op.equals("subtract")) total = num1 - num2;
		if(op.equals("multiply")) total = num1 * num2;
		if(op.equals("divide")) total = num1 / num2;
		
		PrintWriter out = res.getWriter();
		out.println("<p><b>The two numbers you provided are: " + num1 + ", " + num2 + "</p>");
		out.println("<p><b>The operation that you wanted is: " + op + "</p>");
		out.println("<p><b>The result is: " + total + "</p>");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the calculator app</h1>");
		out.println("<p><b>To use the app, input two numbers and an operation. </p>");
		out.println("<p><b>Hit the submit button after filling in the details. </p>");
		out.println("<p><b>You will get the result shown in your browser! </p>");
	}

}
